import autoprefixer from 'autoprefixer';
import babel from '@rollup/plugin-babel';
import commonjs from '@rollup/plugin-commonjs';
import cssnano from 'cssnano';
import postcss from 'postcss';
import resolve from '@rollup/plugin-node-resolve';
import sass from 'rollup-plugin-sass';
import terser from '@rollup/plugin-terser';
import { string } from 'rollup-plugin-string';
import jscc from 'rollup-plugin-jscc';
import replace from '@rollup/plugin-replace';
import { spawnSync } from 'node:child_process';

const getGitTagOrBranch = () => {
  const g1 = spawnSync('git', ['symbolic-ref', '-q', '--short', 'HEAD']).stdout.toString();
  const g2 = spawnSync('git', ['describe', '--tags', '--exact-match']).stdout.toString();
  return (g1 || g2 || process.env.CI_COMMIT_REF_NAME).trim();
}

const gitHash = spawnSync(
  'git', ['rev-parse', '--short', 'HEAD']
).stdout.toString().trim();

const gitTagOrBranch = getGitTagOrBranch();

const production = !process.env.ROLLUP_WATCH;
const cssFile = process.env.CSS_FILE && process.env.CSS_FILE.toLowerCase() === 'true';
const format = process.env.OUTPUT_FORMAT || 'iife';

const input = `src/${format !== 'iife' ? 'index' : 'autolauncher'}.js`;
const output = `public/omnibar${format !== 'iife' ? '.es' : ''}.js`;

export default {
  input,
  output: {
    format,
    file: output,
    sourcemap: true,
    name: format === 'umd' ? 'Omnibar' : null,
    banner: `// omnibar-ng ${gitTagOrBranch} ${gitHash}\n`
  },
  plugins: [
    replace({
      preventAssignment: true,
      "__gitTagOrBranch__": gitTagOrBranch,
      "__gitHash__": gitHash
    }),
    sass({
      output: cssFile,
      processor: css => postcss([autoprefixer(), cssnano])
        .process(css)
        .then(result => result.css)
    }),
    string({
      include: 'src/*.svg'
    }),
    resolve(),
    commonjs(),
    jscc({
      values: { _CSS_FILE: cssFile }
    }),
    babel({babelHelpers: 'bundled'}),
    production && terser({
      output: {
        comments: /^ omnibar-ng/
      }
    })
  ]
};
