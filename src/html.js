const a = () => document.createElement('a');
const div = () => document.createElement('div');
const span = () => document.createElement('span');

export {
  a,
  div,
  span
};
