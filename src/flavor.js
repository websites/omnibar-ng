export default (name, config) => Object
  .prototype.hasOwnProperty.call(config.flavors, name);
