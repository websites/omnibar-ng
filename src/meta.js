const readMetaTag = (name, namespace = 'cznic:omnibar:') => {
  const tag = document.querySelector(`meta[name="${namespace}${name}"]`);
  return tag ? tag.getAttribute('content') : null;
};

export default readMetaTag;
