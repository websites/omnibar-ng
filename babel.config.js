module.exports = {
  presets: ['@babel/env'],
  // "exclude": ["node_modules/**"],
  env: {
    test: {
      presets: [
        [
          '@babel/env',
          {
            modules: 'commonjs'
          }
        ]
      ]
    }
  }
};
