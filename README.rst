omnibar-ng – rewrite of `CZ.NIC omnibar/projectsbar <https://trac.nic.cz/wiki/devel/ginger/omnibar>`_
=========================================================================================================

..

   `DEMO <https://websites.pages.labs.nic.cz/omnibar-ng/>`_



.. image:: readme_images/omnibar.gif
   :target: readme_images/omnibar.gif
   :alt: screencast



* written in ecmascript & scss
* bundled using `rollup <https://github.com/rollup/rollup>`_ & `babel <https://babeljs.io>`_

Sites using it
--------------

Most CZ.NIC sites replaced the old bar with omnibar-ng in August 2019.

Features / comparison with the old omnibar
------------------------------------------

* just a single file, CSS and images are all bundled in javascript *(but it's possible to use external CSS, see below)*
* ~15K (~5K gzipped), UMD build (see below, IIFE build is about 20K including polyfills for IE), old omnibar was about 36K (js + css + images)
* does not `break sites in mobile browsers <https://trac.nic.cz/ticket/20826>`_
* "api" and data files with links compatible with the old bar
* can be controlled programatically
* logs errors via `Sentry <https://sentry.io/>`_ (with a tag) if the containing page is using it
* generates a JS source map on build
* supports modern browsers, plus IE 10 & 11 and the old *non-chrome* Edge (14+)

Basic usage
-----------

.. code-block:: html

   <script src="https://websites.pages.labs.nic.cz/omnibar-ng/omnibar.js"></script>

The widget is available as ``nicWidgets.topBar`` (same as the old omnibar).

autolaunch with defaults:

.. code-block:: javascript

   new nicWidgets.topBar();

Using as an ES module:

.. code-block:: javascript

   import CzNicOmnibar from 'omnibar.es.js';
   const Omnibar = new CzNicOmnibar();

Options
^^^^^^^

.. code-block:: javascript

   new nicWidgets.topBar({
     language, // language to use, 'cs' or 'en' (default: autodetected from document and browser with fallback to English)
     flavor, // JSON file with links to use, files come with the old omnibar, options are 'basic' or 'geek' (default: "basic")
     autolaunch, // bar will mount to body upon creation, without calling .mount() explicitly (default: true)
     opened, // bar will be mounted as expanded (as if user clicked the more/více link) if true (default: false)
     position, // 'top' or 'bottom' sets the position on page (default: 'top')
   });

Old-style options with just a ``language`` and ``flavor`` are supported, too:

.. code-block:: javascript

   new nicWidgets.topBar("cs");

.. code-block:: javascript

   new nicWidgets.topBar("cs", "geek");

Configuration via ``<meta>`` tags
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Compatible with the old omnibar, but with ``cznic:omnibar`` namespace instead of ``cznic:project-bar``\ :

.. code-block:: html

   <meta name="cznic:omnibar:autolaunch" content="on" /> <!-- "on" or "off", maps to true or false obviously -->
   <meta name="cznic:omnibar:flavor" content="geek" /> > <!-- see above -->

Any other values (\ *autolaunch* other than ``on`` or ``off``\ , or *flavor* not found in ``config.js``\ ) will be ignored.

Position can be set via meta tag as well (even though the old bar din't have such option):

.. code-block:: html

   <meta name="cznic:omnibar:position" content="bottom" />

Values configured in meta tags will overwrite these passed in the constructor. So…

.. code-block:: html

   <meta name="cznic:omnibar:position" content="bottom" />
   …
   <script>new nicWidgets.topBar({positon: 'top'});</script>

…will still place the bar at bottom.

Switching from the old omnibar
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

``omnibar-ng`` aims to be compatible with the old omnibar, and should work on most sites after just changing the script URL.

So if you have something like:

.. code-block:: html

   <script type="text/javascript" src="……/projects-bar.js"></script>
   <link rel="stylesheet" type="text/css" href="……/projects-bar.css" />
   <script type="text/javascript">
       new nicWidgets.topBar("cs");
   </script>

…just change the ``.js`` URL and remove the stylesheet link (styles are bundled in ``omnibar.js``\ ):

.. code-block:: html

   <script src="https://websites.pages.labs.nic.cz/omnibar-ng/omnibar.js"></script>
   <script type="text/javascript">
       new nicWidgets.topBar("cs");
   </script>

API
---

``.open``\ , ``.close``\ , ``.toggle``
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Expands/closes the bar, as if user clicked the "more/více/less/méně" link.

.. code-block:: javascript

   const nicBar = new nicWidgets.topBar();
   setTimeout(() => nicBar.open(), 1000);
   setTimeout(() => nicBar.close(), 2000);

Use ``.opened`` to check the current status if needed:

.. code-block:: javascript

   nicBar.opened // → false
   nicBar.open();
   nicBar.opened // → true

``.mount``\ , ``.unmount``
^^^^^^^^^^^^^^^^^^^^^^^^^^

Adds/removes the bar DOM to/from the beginning of ``<body>`` tag. The bar will unmount itself if it fails to fetch the links file.

.. code-block:: javascript

   const nicBar = new nicWidgets.topBar();
   document.querySelector(".some-button").addEventListener("click", () => nicBar.unmount());

``.switchLanguage``
^^^^^^^^^^^^^^^^^^^

Switches the bar language, obviously.

.. code-block:: javascript

   const nicBar = new nicWidgets.topBar("en");
   setTimeout(() => nicBar.switchLanguage("cs"), 1000);
   setTimeout(() => nicBar.switchLanguage("en"), 2000);

It checks whether the language is supported before switching (see ``languages`` in ``config.js``\ ).

``.switchFlavor``
^^^^^^^^^^^^^^^^^

Switches the *flavor*\ , or JSON file to read links from.

.. code-block:: javascript

   const nicBar = new nicWidgets.topBar("en");
   setTimeout(() => nicBar.switchFlavor("geek"), 1000);
   setTimeout(() => nicBar.switchLanguage("basic"), 2000);

Again, it checks if the flavor is supported before switching (see ``flavors`` in ``config.js``\ ).

Changing the links
------------------

Link file format is the same as with old omnibar → see `omnibar wiki page <https://trac.nic.cz/wiki/devel/ginger/omnibar#changethebarscontent>`_.

The *flavors* map to separate JSON files, see ``config.js`` for their URLs. Adding a new one is easy:

.. code-block::

   flavors: {
     basic: 'https://www.nic.cz/files/CORS/projects-bar/basic-datafile.json',
     geek: 'https://www.nic.cz/files/CORS/projects-bar/basic-datafile-geek.json',
     salmon: 'https://foo.bar/salmon.json'
   }

The filename keys are there for compatibility with the old bar, as some sites use(d) eg. ``basic-datafile-geek.json`` instead of just ``geek`` as the flavor name. So both options work thanks to this:

.. code-block::

   {…
     basic: 'https://www.nic.cz/files/CORS/projects-bar/basic-datafile.json',
     'basic-datafile.json': 'https://www.nic.cz/files/CORS/projects-bar/basic-datafile.json',
   …}

Building & development
----------------------

Build as an IIFE (for usage in ``<script src=…>``\ ):

.. code-block:: bash

   npm run build  # outputs omnibar.js

Build an UMD module (for usage in ``import …``\ /\ ``require(…)``\ ):

.. code-block:: bash

   OUTPUT_FORMAT="umd" npm run build  # outputs omnibar.es.js

Linting:

.. code-block:: bash

   npm run lint:js
   npm run lint:css

Running tests:

.. code-block:: bash

   npm run test

Coverage stats are printed to the console and saved to ``coverage/``.

Stylesheets
-----------

Inline CSS
^^^^^^^^^^

Omnibar includes it's styles inline by default.

.. code-block::

   $ npm run build
   …
   $ ls public 
   index.html  omnibar.js  omnibar.js.map

The styles are inserted into ``document.head`` when the omnibar inits:

.. code-block::

   <style id="omnibar-ng" type="text/css">…</style>

External CSS
^^^^^^^^^^^^

However, it's possible to extract the styles to an external file. This could be useful for sites with strict CSP (inline CSS is considered to be *unsafe*\ ):

Just set the ``CSS_FILE`` env. variable to ``true``\ :

.. code-block::

   $ CSS_FILE=true npm run build
   …
   $ ls public 
   index.html  omnibar.css  omnibar.js  omnibar.js.map
               ‾‾‾‾‾‾‾‾‾‾‾

The bar will try to insert link to the stylesheet, deriving it's location from the script URL itself (replacing script name with ``omnibar.css``\ ).
If you are loading the script via ``<script src="https://webserver/path/to/omnibar.js"></script>``\ , the stylesheet URL would be ``https://webserver/path/to/omnibar.css``. The JS filename doesn't influence the CSS filename – ``<script src="URL/omnibar.d668b3744271.js">`` or even ``<script src="URL/foo-blah.js">`` will still result in ``URL/omnibar.css``\ :

.. code-block::

   <link id="omnibar-ng" rel="stylesheet" href="https://…/omnibar.css">

CSS namespace
^^^^^^^^^^^^^

If you need a different "namespace" (base class name), just change the ``baseCss`` option in ``src/config.js`` before building:

.. code-block::

   baseCss: 'cznic-omnibar'

CI
--

Linters, tests, and production build run in GitLab CI after each push. Tags are deployed to GitLab pages, which provide the linked *DEMO* page (built with inline CSS, see above for details).

Issues or feature requests
--------------------------

Please use issues in `this GitLab repository <https://gitlab.labs.nic.cz/websites/omnibar-ng/issues>`_.
