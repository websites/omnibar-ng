const esModules = ['@helb/script-url'];

module.exports = {
  collectCoverage: true,
  testEnvironment: 'jsdom',
  coverageDirectory: 'coverage',
  setupFiles: ['<rootDir>/test/__mocks__/client.js'],
  moduleFileExtensions: ['js'],
  transformIgnorePatterns: [`<rootDir>/node_modules/(?!${esModules.join('|')})`],
  moduleNameMapper: {
    '\\.(css|less|sass|scss)$': '<rootDir>/test/__mocks__/style.js',
    '\\.(gif|ttf|eot|svg)$': '<rootDir>/test/__mocks__/file.js'
  }
};
