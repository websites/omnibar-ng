import { a, div, span } from '../src/html';

test('a() returns html element', () => {
  expect(a() instanceof HTMLElement).toBe(true);
  expect(a().outerHTML).toBe('<a></a>');
});

test('div() returns html element', () => {
  expect(div() instanceof HTMLElement).toBe(true);
  expect(div().outerHTML).toBe('<div></div>');
});

test('span() returns html element', () => {
  expect(span() instanceof HTMLElement).toBe(true);
  expect(span().outerHTML).toBe('<span></span>');
});
