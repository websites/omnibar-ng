import CzNicOmnibar from '../src';

test('Omnibar doesnt start opened when not asked to', () => {
  document.body.innerHTML = '';
  const bar = new CzNicOmnibar({ opened: false });
  expect(bar.opened).toBe(false);
  expect(bar.rootElement.classList.contains('cznic-omnibar-root-closed')).toBe(true);
});

test('Omnibar starts opened when asked to', () => {
  document.body.innerHTML = '';
  const bar = new CzNicOmnibar({ opened: true });
  expect(bar.opened).toBe(true);
  expect(bar.rootElement.classList.contains('cznic-omnibar-root-opened')).toBe(true);
});

test('Omnibar opens and closes as expected with positiom: top', () => {
  document.body.innerHTML = '';
  const bar = new CzNicOmnibar({ opened: false });
  expect(bar.opened).toBe(false);
  expect(bar.rootElement.classList.contains('cznic-omnibar-root-closed')).toBe(true);
  bar.open();
  expect(bar.opened).toBe(true);
  expect(bar.rootElement.classList.contains('cznic-omnibar-root-opened')).toBe(true);
  bar.close();
  expect(bar.opened).toBe(false);
  expect(bar.rootElement.classList.contains('cznic-omnibar-root-closed')).toBe(true);
  bar.toggle();
  expect(bar.opened).toBe(true);
  expect(bar.rootElement.classList.contains('cznic-omnibar-root-opened')).toBe(true);
  bar.toggle();
  expect(bar.opened).toBe(false);
  expect(bar.rootElement.classList.contains('cznic-omnibar-root-closed')).toBe(true);
});

test('Omnibar opens and closes as expected with positiom: bottom', () => {
  document.body.innerHTML = '';
  const bar = new CzNicOmnibar({ opened: false, position: 'bottom' });
  expect(bar.opened).toBe(false);
  expect(bar.rootElement.classList.contains('cznic-omnibar-root-closed')).toBe(true);
  bar.open();
  expect(bar.opened).toBe(true);
  expect(bar.rootElement.classList.contains('cznic-omnibar-root-opened')).toBe(true);
  bar.close();
  expect(bar.opened).toBe(false);
  expect(bar.rootElement.classList.contains('cznic-omnibar-root-closed')).toBe(true);
  bar.toggle();
  expect(bar.opened).toBe(true);
  expect(bar.rootElement.classList.contains('cznic-omnibar-root-opened')).toBe(true);
  bar.toggle();
  expect(bar.opened).toBe(false);
  expect(bar.rootElement.classList.contains('cznic-omnibar-root-closed')).toBe(true);
});

test('Omnibar uses a default "opened" value when passed a non-boolean', () => {
  document.body.innerHTML = '';
  const bar = new CzNicOmnibar({ opened: 'foo' });
  expect(bar.opened).toEqual(false);
});
