import { omnibarLink, getLinkHref, getLinkText } from '../src/omnibarLink';

// link file format docs: https://trac.nic.cz/wiki/devel/ginger/omnibar#changethebarscontent

const link1 = ['title_cs', 'title_en', 'url_cs', 'url_en'];
const link2 = ['title_cs', 'title_en', 'url_cs', ''];
const link3 = ['title_cs', 'title_en', 'url_cs'];
const empty = [];

test('getLinkHref() returns correct values', () => {
  expect(getLinkHref(link1, 'cs')).toBe('url_cs');
  expect(getLinkHref(link1, 'en')).toBe('url_en');
  expect(getLinkHref(link2, 'en')).toBe('url_cs');
  expect(getLinkHref(link3, 'en')).toBe('url_cs');
  expect(getLinkHref(empty, 'en')).toBe(undefined);
});

test('getLinkText() returns correct values', () => {
  expect(getLinkText(link1, 'cs')).toBe('title_cs');
  expect(getLinkText(link1, 'en')).toBe('title_en');
  expect(getLinkText(link2, 'en')).toBe('title_en');
  expect(getLinkText(link3, 'en')).toBe('title_en');
  expect(getLinkText(empty, 'en')).toBe(undefined);
});

test('omnibarLink() returns html element when given data', () => {
  expect(omnibarLink(link1, 'cs') instanceof HTMLElement).toBe(true);
});

test('omnibarLink() sets correct href and text', () => {
  expect(omnibarLink(link1, 'cs').querySelector('a').innerText).toBe('title_cs');
  expect(omnibarLink(link1, 'cs').querySelector('a').href).toMatch(/.*url_cs$/);
  expect(omnibarLink(link1, 'en').querySelector('a').innerText).toBe('title_en');
  expect(omnibarLink(link1, 'en').querySelector('a').href).toMatch(/.*url_en$/);
  expect(omnibarLink(link2, 'en').querySelector('a').innerText).toBe('title_en');
  expect(omnibarLink(link2, 'en').querySelector('a').href).toMatch(/.*url_cs$/);
  expect(omnibarLink(link3, 'en').querySelector('a').innerText).toBe('title_en');
  expect(omnibarLink(link3, 'en').querySelector('a').href).toMatch(/.*url_cs$/);
});

test('omnibarLink() returns null with no data', () => {
  expect(omnibarLink(empty, 'en')).toBe(null);
});
