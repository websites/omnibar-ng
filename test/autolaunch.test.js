import CzNicOmnibar from '../src';

test('Omnibar automounts when asked to', () => {
  document.body.innerHTML = '';
  const bar = new CzNicOmnibar({ autolaunch: true });
  expect(bar.autolaunch).toEqual(true);
  expect(bar.rootElement).toEqual(document.querySelector('body *:first-child'));
});

test('Omnibar doesnt automount when not asked to', () => {
  document.body.innerHTML = '';
  const bar = new CzNicOmnibar({ autolaunch: false });
  expect(bar.autolaunch).toEqual(false);
  expect(document.body.innerHTML).toEqual('');
});

test('Omnibar uses a default autolaunch value when passed a non-boolean', () => {
  document.body.innerHTML = '';
  const bar = new CzNicOmnibar({ autolaunch: 'foo' });
  expect(bar.autolaunch).toEqual(true);
});

test('Omnibar reads autolaunch from meta tag', () => {
  document.head.innerHTML = '<meta name="cznic:omnibar:autolaunch" content="off" />';
  document.body.innerHTML = '';
  const bar1 = new CzNicOmnibar();
  expect(bar1.autolaunch).toEqual(false);
  document.head.innerHTML = '<meta name="cznic:omnibar:autolaunch" content="on" />';
  document.body.innerHTML = '';
  const bar2 = new CzNicOmnibar();
  expect(bar2.autolaunch).toEqual(true);
});
