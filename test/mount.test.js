import CzNicOmnibar from '../src';

test('Omnibar mounts when asked to', () => {
  document.body.innerHTML = '';
  const bar = new CzNicOmnibar({ autolaunch: false });
  bar.mount();
  expect(bar.rootElement).toEqual(document.querySelector('body *:first-child'));
  bar.unmount();
});

test('Omnibar unmounts when asked to', () => {
  document.body.innerHTML = '';
  const bar = new CzNicOmnibar({ autolaunch: true });
  bar.unmount();
  expect(document.body.innerHTML).toEqual('');
});
