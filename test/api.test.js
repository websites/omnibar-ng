import CzNicOmnibar from '../src';

test('Omnibar can be imported', () => {
  expect(typeof CzNicOmnibar.constructor).toBe('function');
});

test('Omnibar can be invoked using new … and creates a HTMLElement', () => {
  document.body.innerHTML = '';
  const bar = new CzNicOmnibar();
  expect(typeof bar).toBe('object');
  expect(bar.rootElement instanceof HTMLElement).toBe(true);
});

test('Omnibar can be invoked using the old-style language option', () => {
  document.body.innerHTML = '';
  const bar = new CzNicOmnibar('cs');
  expect(bar.language).toBe('cs');
});

test('Omnibar can be invoked using the old-style language and flavor options', () => {
  document.body.innerHTML = '';
  const bar = new CzNicOmnibar('cs', 'geek');
  expect(bar.language).toBe('cs');
  expect(bar.flavor).toBe('geek');
});
