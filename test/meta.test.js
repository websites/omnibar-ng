import readMetaTag from '../src/meta';

document.documentElement.innerHTML = `
  <meta name='cznic:omnibar:autolaunch' content='on'>
  <meta name='cznic:omnibar:flavor' content='basic'>`;

test('readMetaTag reads tag content correctly', () => {
  expect(readMetaTag('autolaunch')).toBe('on');
  expect(readMetaTag('flavor')).toBe('basic');
});

test('readMetaTag returns null for non-existing tags', () => {
  expect(readMetaTag('xx')).toBe(null);
});
