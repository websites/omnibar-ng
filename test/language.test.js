import { isLanguageSupported, browserLanguage } from '../src/language.js';
import CzNicOmnibar from '../src';
import config from '../src/config';

test('isLanguageSupported returns true for supported language', () => {
  expect(isLanguageSupported('cs', config)).toBe(true);
  expect(isLanguageSupported('en', config)).toBe(true);
});

test('isLanguageSupported returns false for unsupported language', () => {
  expect(isLanguageSupported('xx', config)).toBe(false);
});

test('browserLanguage returns the document language if supported', () => {
  document.documentElement.lang = 'en';
  expect(browserLanguage(config)).toBe('en');
});

test('browserLanguage returns the fallback language if document language is unsupported', () => {
  document.documentElement.lang = 'xx';
  expect(browserLanguage(config)).toBe(config.fallbackLanguage);
});

test('browserLanguage prefers the document language (if supported) over browser language', () => {
  Object.defineProperty(window.navigator, 'language', { value: 'en', configurable: true });
  document.documentElement.lang = 'cs';
  expect(browserLanguage(config)).toBe('cs');
});

test('browserLanguage returns the browser language if document language is unsupported', () => {
  Object.defineProperty(window.navigator, 'language', { value: 'cs', configurable: true });
  document.documentElement.lang = 'xx';
  expect(browserLanguage(config)).toBe('cs');
});

test('browserLanguage falls back to default if both document & browser languages are unsupported', () => {
  Object.defineProperty(window.navigator, 'language', { value: 'xx', configurable: true });
  document.documentElement.lang = 'xx';
  expect(browserLanguage(config)).toBe(config.fallbackLanguage);
});

test('bar uses the default language when an unsupported one is requested', () => {
  document.body.innerHTML = '';
  const bar = new CzNicOmnibar({ language: 'xx' });
  expect(bar.language).toEqual(config.fallbackLanguage);
  bar.switchLanguage('xy');
  expect(bar.language).toEqual(config.fallbackLanguage);
});

test('switchLanguage works', () => {
  document.body.innerHTML = '';
  const bar = new CzNicOmnibar({ language: 'en' });
  expect(bar.language).toEqual('en');
  bar.switchLanguage('cs');
  expect(bar.language).toEqual('cs');
});
