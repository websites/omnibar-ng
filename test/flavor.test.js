import isFlavorSupported from '../src/flavor';
import CzNicOmnibar from '../src';

const config = {
  flavors: {
    a: null,
    b: null
  }
};

test('isFlavorSupported returns true for supported flavor', () => {
  expect(isFlavorSupported('a', config)).toBe(true);
  expect(isFlavorSupported('b', config)).toBe(true);
});

test('isFlavorSupported returns false for unsupported flavor', () => {
  expect(isFlavorSupported('c', config)).toBe(false);
});

test('Omnibar uses a default flavor when asked for an unknown one', () => {
  document.body.innerHTML = '';
  const bar = new CzNicOmnibar({ autolaunch: true, flavor: 'foo' });
  expect(bar.flavor).toEqual('basic');
});

test('Omnibar can switch flavors', () => {
  document.body.innerHTML = '';
  const bar = new CzNicOmnibar({ autolaunch: true, flavor: 'basic' });
  expect(bar.flavor).toEqual('basic');
  bar.switchFlavor('geek');
  expect(bar.flavor).toEqual('geek');
  bar.switchFlavor('foo');
  expect(bar.flavor).toEqual('geek');
});

test('Omnibar reads flavor from meta tag', () => {
  document.head.innerHTML = '<meta name="cznic:omnibar:flavor" content="geek" />';
  document.body.innerHTML = '';
  const bar = new CzNicOmnibar();
  expect(bar.flavor).toEqual('geek');
});
