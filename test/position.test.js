import CzNicOmnibar from '../src';

test('position defaults to top', () => {
  const bar = new CzNicOmnibar();
  expect(bar.position).toEqual('top');
});

test('Omnibar mounts to a start of <body> with position: top', () => {
  document.body.innerHTML = '<p>blah</p><p>blah</p>';
  const bar = new CzNicOmnibar({ autolaunch: true, position: 'top' });
  expect(bar.rootElement).toEqual(document.querySelector('body *:first-child'));
});

test('Omnibar mounts to an end of <body> with position: bottom', () => {
  document.body.innerHTML = '<p>blah</p><p>blah</p>';
  const bar = new CzNicOmnibar({ autolaunch: true, position: 'bottom' });
  expect(bar.rootElement).toEqual(document.querySelector('body *:last-child'));
});

test('Omnibar uses a default positon when passed an unknown one', () => {
  document.body.innerHTML = '';
  const bar = new CzNicOmnibar({ position: 'foo' });
  expect(bar.position).toEqual('top');
});

test('Omnibar reads positon from meta tag', () => {
  document.head.innerHTML = '<meta name="cznic:omnibar:position" content="bottom" />';
  document.body.innerHTML = '';
  const bar = new CzNicOmnibar();
  expect(bar.position).toEqual('bottom');
});
