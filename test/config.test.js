import config from '../src/config';

test('config contains required keys', () => {
  expect(Object.keys(config.flavors).length).toBeGreaterThan(1);
  expect(typeof config.logoLink).toBe('string');
  expect(typeof config.fallbackLanguage).toBe('string');
  expect(Object.keys(config.languages).length).toBeGreaterThan(1);
  Object.keys(config.languages).forEach(lang => expect(
    Object.keys(config.languages[lang]).sort()
  ).toStrictEqual([
    'jump',
    'less',
    'more'
  ]));
  expect(typeof config.baseCss).toBe('string');
});
